import React from "react";
import { Redirect, Switch } from "react-router-dom";
import AuthRoutes from "routes/AuthRoutes";
import UnAuthRoutes from "routes/UnAuthRoutes";
import DashboardLayout from "components/layouts/DashboardLayout";
import Login from "components/session/login";

const Routes = () => {
  return (
    <Switch>
      <AuthRoutes path={"/dashboard"} component={DashboardLayout} />
      <UnAuthRoutes path={"/login"} component={Login} />
      <Redirect from="" to="/login" />
    </Switch>
  );
};

export default Routes;
