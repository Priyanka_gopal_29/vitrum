import { LoginUrl } from "./URL";

export const Login = (headers = {}, params = {}) => {
  return Axios.get(LoginUrl.SignIn, {
    headers: headers,
    params: params
  });
};
