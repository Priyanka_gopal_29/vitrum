import React from "react";
import { useHistory } from "react-router-dom";

const Login = () => {
  const history = useHistory();
  return (
    <div>
      <button
        onClick={() => {
          localStorage.setItem("token", "35007");
          history.push("/dashboard");
        }}
      >
        Login
      </button>
    </div>
  );
};
export default Login;
