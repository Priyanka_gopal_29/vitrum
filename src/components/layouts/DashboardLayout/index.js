import React from "react";
import { Route, Switch } from "react-router-dom";

export const test = () => {
  return "test";
};
export const test1 = () => {
  return "test1";
};

const DashboardLayout = () => {
  return (
    <div>
      <div>header</div>
      <div>
        Router Area{" "}
        <Switch>
          <Route path="/dashboard/test" component={test} />
        </Switch>
        <Switch>
          <Route path="/dashboard/test1" component={test1} />
        </Switch>
      </div>
      <div>Footer</div>
    </div>
  );
};
export default DashboardLayout;
