import React from "react";
import { BrowserRouter } from "react-router-dom";
// import { Provider } from "react-redux";
import "./App.css";
import ScrollToTop from "utils/ScrollToTop";
import Routes from "routes";

// const store = configureStore();

const App = () => {
  return (
    // <Provider store={store}>
    <BrowserRouter basename="/">
      <ScrollToTop>
        <Routes />
      </ScrollToTop>
    </BrowserRouter>
    // </Provider>
  );
};

export default App;
